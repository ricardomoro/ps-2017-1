<?php
// Habilita a personalização de cabeçalho
add_theme_support('custom-header', array(
    'default-image'          => '',
	'width'                  => 1008,
	'height'                 => 320,
	'flex-height'            => false,
	'flex-width'             => false,
	'uploads'                => true,
	'random-default'         => false,
	'header-text'            => false,
	'default-text-color'     => '',
	'wp-head-callback'       => '',
	'admin-head-callback'    => '',
	'admin-preview-callback' => '',
));
