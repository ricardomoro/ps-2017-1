<div class="row">
    <div class="col-xs-12">
        <div class="alert alert-warning" role="alert">
            <p><strong>*</strong> Para saber mais sobre a forma de distribui&ccedil;&atilde;o das vagas, leia os <a href="<?php echo get_post_type_archive_link( 'edital' ); ?>">editais</a>.</p>
        </div>
    </div>
</div>
